import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:material_theme_builder/src/material_font.dart';
import 'package:material_theme_builder/src/serializers.dart';

part 'material_font_style.g.dart';

abstract class MaterialFontStyle
    implements Built<MaterialFontStyle, MaterialFontStyleBuilder> {
  static Serializer<MaterialFontStyle> get serializer =>
      _$materialFontStyleSerializer;

  String get name;
  MaterialFont get large;
  MaterialFont get medium;
  MaterialFont get small;
  List<MaterialFont> get fonts => [large, medium, small];

  factory MaterialFontStyle([Function(MaterialFontStyleBuilder b) updates]) =
      _$MaterialFontStyle;

  MaterialFontStyle._();

  factory MaterialFontStyle.instance() {
    return MaterialFontStyle((b) => b);
  }

  factory MaterialFontStyle.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json)!;
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }

  String fontStyle(MaterialFont font) {
    if (font == large) {
      return '${name}Large';
    }
    if (font == medium) {
      return '${name}Medium';
    }
    if (font == small) {
      return '${name}Small';
    }
    return '';
  }
}
