import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:material_theme_builder/src/serializers.dart';

part 'material_selection.g.dart';

abstract class MaterialSelection
    implements Built<MaterialSelection, MaterialSelectionBuilder> {
  static Serializer<MaterialSelection> get serializer =>
      _$materialSelectionSerializer;

  String? get id;

  int get value;
  int get onValue;
  int get valueContainer;
  int get onValueContainer;

  factory MaterialSelection([Function(MaterialSelectionBuilder b) updates]) =
      _$MaterialSelection;

  MaterialSelection._();

  factory MaterialSelection.instance() {
    return MaterialSelection((b) => b);
  }

  factory MaterialSelection.fromJson(Map json, {String? id}) {
    return serializers
        .deserializeWith(serializer, json)!
        .rebuild((b) => b.id = id);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
