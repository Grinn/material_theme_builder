// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'material_font_style.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<MaterialFontStyle> _$materialFontStyleSerializer =
    new _$MaterialFontStyleSerializer();

class _$MaterialFontStyleSerializer
    implements StructuredSerializer<MaterialFontStyle> {
  @override
  final Iterable<Type> types = const [MaterialFontStyle, _$MaterialFontStyle];
  @override
  final String wireName = 'MaterialFontStyle';

  @override
  Iterable<Object?> serialize(Serializers serializers, MaterialFontStyle object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'large',
      serializers.serialize(object.large,
          specifiedType: const FullType(MaterialFont)),
      'medium',
      serializers.serialize(object.medium,
          specifiedType: const FullType(MaterialFont)),
      'small',
      serializers.serialize(object.small,
          specifiedType: const FullType(MaterialFont)),
    ];

    return result;
  }

  @override
  MaterialFontStyle deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MaterialFontStyleBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'large':
          result.large.replace(serializers.deserialize(value,
              specifiedType: const FullType(MaterialFont))! as MaterialFont);
          break;
        case 'medium':
          result.medium.replace(serializers.deserialize(value,
              specifiedType: const FullType(MaterialFont))! as MaterialFont);
          break;
        case 'small':
          result.small.replace(serializers.deserialize(value,
              specifiedType: const FullType(MaterialFont))! as MaterialFont);
          break;
      }
    }

    return result.build();
  }
}

class _$MaterialFontStyle extends MaterialFontStyle {
  @override
  final String name;
  @override
  final MaterialFont large;
  @override
  final MaterialFont medium;
  @override
  final MaterialFont small;

  factory _$MaterialFontStyle(
          [void Function(MaterialFontStyleBuilder)? updates]) =>
      (new MaterialFontStyleBuilder()..update(updates)).build();

  _$MaterialFontStyle._(
      {required this.name,
      required this.large,
      required this.medium,
      required this.small})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(name, 'MaterialFontStyle', 'name');
    BuiltValueNullFieldError.checkNotNull(large, 'MaterialFontStyle', 'large');
    BuiltValueNullFieldError.checkNotNull(
        medium, 'MaterialFontStyle', 'medium');
    BuiltValueNullFieldError.checkNotNull(small, 'MaterialFontStyle', 'small');
  }

  @override
  MaterialFontStyle rebuild(void Function(MaterialFontStyleBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MaterialFontStyleBuilder toBuilder() =>
      new MaterialFontStyleBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MaterialFontStyle &&
        name == other.name &&
        large == other.large &&
        medium == other.medium &&
        small == other.small;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, name.hashCode), large.hashCode), medium.hashCode),
        small.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MaterialFontStyle')
          ..add('name', name)
          ..add('large', large)
          ..add('medium', medium)
          ..add('small', small))
        .toString();
  }
}

class MaterialFontStyleBuilder
    implements Builder<MaterialFontStyle, MaterialFontStyleBuilder> {
  _$MaterialFontStyle? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  MaterialFontBuilder? _large;
  MaterialFontBuilder get large => _$this._large ??= new MaterialFontBuilder();
  set large(MaterialFontBuilder? large) => _$this._large = large;

  MaterialFontBuilder? _medium;
  MaterialFontBuilder get medium =>
      _$this._medium ??= new MaterialFontBuilder();
  set medium(MaterialFontBuilder? medium) => _$this._medium = medium;

  MaterialFontBuilder? _small;
  MaterialFontBuilder get small => _$this._small ??= new MaterialFontBuilder();
  set small(MaterialFontBuilder? small) => _$this._small = small;

  MaterialFontStyleBuilder();

  MaterialFontStyleBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _large = $v.large.toBuilder();
      _medium = $v.medium.toBuilder();
      _small = $v.small.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MaterialFontStyle other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MaterialFontStyle;
  }

  @override
  void update(void Function(MaterialFontStyleBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$MaterialFontStyle build() {
    _$MaterialFontStyle _$result;
    try {
      _$result = _$v ??
          new _$MaterialFontStyle._(
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'MaterialFontStyle', 'name'),
              large: large.build(),
              medium: medium.build(),
              small: small.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'large';
        large.build();
        _$failedField = 'medium';
        medium.build();
        _$failedField = 'small';
        small.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'MaterialFontStyle', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
