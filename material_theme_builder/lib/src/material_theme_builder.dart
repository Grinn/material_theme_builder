import 'dart:async';
import 'dart:convert';

import 'package:build/build.dart';
import 'package:material_theme_builder/src/material_theme.dart';
import 'package:material_theme_builder/src/material_theme_generator.dart';

class MaterialThemeBuilder extends Builder {
  final MaterialThemeGenerator generator;

  MaterialThemeBuilder(this.generator);

  @override
  FutureOr<void> build(BuildStep buildStep) async {
    try {
      final _source = await buildStep.readAsString(buildStep.inputId);
      final _json = jsonDecode(_source) as Map<String, dynamic>? ?? {};
      final _theme = MaterialTheme.fromJson(_json);
      final _themeAsDartClass = await generator.generateDartTheme(_theme);
      final _themeAsScss = await generator.generateScssTheme(_theme);
      final _colorsMixinScss = await generator.generateColorsMixin(_theme);
      final _textMixinScss = await generator.generateTextMixin(_theme);
      final _filename = buildStep.inputId.path.split('/').last.split('.').first;
      final _dartAssetId = AssetId(
          buildStep.inputId.package,
          buildStep.inputId.path.replaceFirst(
              buildStep.inputId.path.split('/').last,
              '${_filename}.theme.dart'));
      final _scssAssetId = AssetId(
          buildStep.inputId.package,
          buildStep.inputId.path.replaceFirst(
              buildStep.inputId.path.split('/').last,
              '${_filename}.theme.scss'));
      final _colorsAssetId = AssetId(
          buildStep.inputId.package,
          buildStep.inputId.path.replaceFirst(
              buildStep.inputId.path.split('/').last,
              '${_filename}.colors.scss'));
      final _textsAssetId = AssetId(
          buildStep.inputId.package,
          buildStep.inputId.path.replaceFirst(
              buildStep.inputId.path.split('/').last,
              '${_filename}.texts.scss'));
      buildStep.writeAsString(_dartAssetId, _themeAsDartClass!);
      buildStep.writeAsString(_scssAssetId, _themeAsScss!);
      buildStep.writeAsString(_colorsAssetId, _colorsMixinScss!);
      buildStep.writeAsString(_textsAssetId, _textMixinScss!);
    } catch (e, stacktrace) {
      print(e);
      print(stacktrace);
    }
  }

  @override
  Map<String, List<String>> get buildExtensions => {
        'theme.json': ['theme.dart', 'theme.scss', 'colors.scss', 'texts.scss']
      };
}
