import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:material_theme_builder/src/serializers.dart';

part 'material_font.g.dart';

abstract class MaterialFont
    implements Built<MaterialFont, MaterialFontBuilder> {
  static Serializer<MaterialFont> get serializer => _$materialFontSerializer;

  String get family;
  String get size;
  String get weight;
  String get lineHeight;
  String get tracking;
  String? get isDefault;

  factory MaterialFont([Function(MaterialFontBuilder b) updates]) =
      _$MaterialFont;

  MaterialFont._();

  factory MaterialFont.instance() {
    return MaterialFont((b) => b);
  }

  factory MaterialFont.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json)!;
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
