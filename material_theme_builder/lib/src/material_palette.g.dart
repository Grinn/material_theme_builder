// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'material_palette.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<MaterialPalette> _$materialPaletteSerializer =
    new _$MaterialPaletteSerializer();

class _$MaterialPaletteSerializer
    implements StructuredSerializer<MaterialPalette> {
  @override
  final Iterable<Type> types = const [MaterialPalette, _$MaterialPalette];
  @override
  final String wireName = 'MaterialPalette';

  @override
  Iterable<Object?> serialize(Serializers serializers, MaterialPalette object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'value',
      serializers.serialize(object.value,
          specifiedType: const FullType(String)),
      'light',
      serializers.serialize(object.light,
          specifiedType: const FullType(MaterialSelection)),
      'dark',
      serializers.serialize(object.dark,
          specifiedType: const FullType(MaterialSelection)),
    ];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  MaterialPalette deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MaterialPaletteBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'light':
          result.light.replace(serializers.deserialize(value,
                  specifiedType: const FullType(MaterialSelection))!
              as MaterialSelection);
          break;
        case 'dark':
          result.dark.replace(serializers.deserialize(value,
                  specifiedType: const FullType(MaterialSelection))!
              as MaterialSelection);
          break;
      }
    }

    return result.build();
  }
}

class _$MaterialPalette extends MaterialPalette {
  @override
  final String? id;
  @override
  final String name;
  @override
  final String value;
  @override
  final MaterialSelection light;
  @override
  final MaterialSelection dark;

  factory _$MaterialPalette([void Function(MaterialPaletteBuilder)? updates]) =>
      (new MaterialPaletteBuilder()..update(updates)).build();

  _$MaterialPalette._(
      {this.id,
      required this.name,
      required this.value,
      required this.light,
      required this.dark})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(name, 'MaterialPalette', 'name');
    BuiltValueNullFieldError.checkNotNull(value, 'MaterialPalette', 'value');
    BuiltValueNullFieldError.checkNotNull(light, 'MaterialPalette', 'light');
    BuiltValueNullFieldError.checkNotNull(dark, 'MaterialPalette', 'dark');
  }

  @override
  MaterialPalette rebuild(void Function(MaterialPaletteBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MaterialPaletteBuilder toBuilder() =>
      new MaterialPaletteBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MaterialPalette &&
        id == other.id &&
        name == other.name &&
        value == other.value &&
        light == other.light &&
        dark == other.dark;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, id.hashCode), name.hashCode), value.hashCode),
            light.hashCode),
        dark.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MaterialPalette')
          ..add('id', id)
          ..add('name', name)
          ..add('value', value)
          ..add('light', light)
          ..add('dark', dark))
        .toString();
  }
}

class MaterialPaletteBuilder
    implements Builder<MaterialPalette, MaterialPaletteBuilder> {
  _$MaterialPalette? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _value;
  String? get value => _$this._value;
  set value(String? value) => _$this._value = value;

  MaterialSelectionBuilder? _light;
  MaterialSelectionBuilder get light =>
      _$this._light ??= new MaterialSelectionBuilder();
  set light(MaterialSelectionBuilder? light) => _$this._light = light;

  MaterialSelectionBuilder? _dark;
  MaterialSelectionBuilder get dark =>
      _$this._dark ??= new MaterialSelectionBuilder();
  set dark(MaterialSelectionBuilder? dark) => _$this._dark = dark;

  MaterialPaletteBuilder();

  MaterialPaletteBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _value = $v.value;
      _light = $v.light.toBuilder();
      _dark = $v.dark.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MaterialPalette other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MaterialPalette;
  }

  @override
  void update(void Function(MaterialPaletteBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$MaterialPalette build() {
    _$MaterialPalette _$result;
    try {
      _$result = _$v ??
          new _$MaterialPalette._(
              id: id,
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'MaterialPalette', 'name'),
              value: BuiltValueNullFieldError.checkNotNull(
                  value, 'MaterialPalette', 'value'),
              light: light.build(),
              dark: dark.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'light';
        light.build();
        _$failedField = 'dark';
        dark.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'MaterialPalette', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
