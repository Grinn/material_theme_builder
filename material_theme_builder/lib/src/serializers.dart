import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:material_theme_builder/src/material_font.dart';
import 'package:material_theme_builder/src/material_font_style.dart';
import 'package:material_theme_builder/src/material_palette.dart';
import 'package:material_theme_builder/src/material_selection.dart';

part 'serializers.g.dart';

@SerializersFor(const [
  MaterialPalette,
  MaterialSelection,
  MaterialFont,
  MaterialFontStyle,
])
final Serializers serializers =
    (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
