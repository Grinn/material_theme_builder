import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:material_color_utilities/material_color_utilities.dart';
import 'package:material_theme_builder/src/material_selection.dart';
import 'package:material_theme_builder/src/serializers.dart';

part 'material_palette.g.dart';

abstract class MaterialPalette
    implements Built<MaterialPalette, MaterialPaletteBuilder> {
  static Serializer<MaterialPalette> get serializer =>
      _$materialPaletteSerializer;

  String? get id;

  String get name;
  String get value;
  MaterialSelection get light;
  MaterialSelection get dark;

  Map<dynamic, dynamic> get tones {
    final _palette = {};
    final _valueAsInt = value.replaceFirst('#', '0xff');
    final _hctColor = Hct.fromInt(int.parse(_valueAsInt));
    final _tonalPalette = TonalPalette.of(_hctColor.hue, _hctColor.chroma);
    for (int tone in TonalPalette.commonTones) {
      final _color = _tonalPalette.get(tone).toRadixString(16);
      _palette[tone] = {
        'int': '0x$_color',
        'hex': '#${_color.substring(2)}',
      };
    }
    return _palette;
  }

  factory MaterialPalette([Function(MaterialPaletteBuilder b) updates]) =
      _$MaterialPalette;

  MaterialPalette._();

  factory MaterialPalette.instance() {
    return MaterialPalette((b) => b);
  }

  factory MaterialPalette.fromJson(Map json, {String? id}) {
    return serializers
        .deserializeWith(serializer, json)!
        .rebuild((b) => b.id = id);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
