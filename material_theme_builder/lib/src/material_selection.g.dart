// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'material_selection.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<MaterialSelection> _$materialSelectionSerializer =
    new _$MaterialSelectionSerializer();

class _$MaterialSelectionSerializer
    implements StructuredSerializer<MaterialSelection> {
  @override
  final Iterable<Type> types = const [MaterialSelection, _$MaterialSelection];
  @override
  final String wireName = 'MaterialSelection';

  @override
  Iterable<Object?> serialize(Serializers serializers, MaterialSelection object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'value',
      serializers.serialize(object.value, specifiedType: const FullType(int)),
      'onValue',
      serializers.serialize(object.onValue, specifiedType: const FullType(int)),
      'valueContainer',
      serializers.serialize(object.valueContainer,
          specifiedType: const FullType(int)),
      'onValueContainer',
      serializers.serialize(object.onValueContainer,
          specifiedType: const FullType(int)),
    ];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  MaterialSelection deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MaterialSelectionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'onValue':
          result.onValue = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'valueContainer':
          result.valueContainer = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'onValueContainer':
          result.onValueContainer = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$MaterialSelection extends MaterialSelection {
  @override
  final String? id;
  @override
  final int value;
  @override
  final int onValue;
  @override
  final int valueContainer;
  @override
  final int onValueContainer;

  factory _$MaterialSelection(
          [void Function(MaterialSelectionBuilder)? updates]) =>
      (new MaterialSelectionBuilder()..update(updates)).build();

  _$MaterialSelection._(
      {this.id,
      required this.value,
      required this.onValue,
      required this.valueContainer,
      required this.onValueContainer})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(value, 'MaterialSelection', 'value');
    BuiltValueNullFieldError.checkNotNull(
        onValue, 'MaterialSelection', 'onValue');
    BuiltValueNullFieldError.checkNotNull(
        valueContainer, 'MaterialSelection', 'valueContainer');
    BuiltValueNullFieldError.checkNotNull(
        onValueContainer, 'MaterialSelection', 'onValueContainer');
  }

  @override
  MaterialSelection rebuild(void Function(MaterialSelectionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MaterialSelectionBuilder toBuilder() =>
      new MaterialSelectionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MaterialSelection &&
        id == other.id &&
        value == other.value &&
        onValue == other.onValue &&
        valueContainer == other.valueContainer &&
        onValueContainer == other.onValueContainer;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, id.hashCode), value.hashCode), onValue.hashCode),
            valueContainer.hashCode),
        onValueContainer.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MaterialSelection')
          ..add('id', id)
          ..add('value', value)
          ..add('onValue', onValue)
          ..add('valueContainer', valueContainer)
          ..add('onValueContainer', onValueContainer))
        .toString();
  }
}

class MaterialSelectionBuilder
    implements Builder<MaterialSelection, MaterialSelectionBuilder> {
  _$MaterialSelection? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _value;
  int? get value => _$this._value;
  set value(int? value) => _$this._value = value;

  int? _onValue;
  int? get onValue => _$this._onValue;
  set onValue(int? onValue) => _$this._onValue = onValue;

  int? _valueContainer;
  int? get valueContainer => _$this._valueContainer;
  set valueContainer(int? valueContainer) =>
      _$this._valueContainer = valueContainer;

  int? _onValueContainer;
  int? get onValueContainer => _$this._onValueContainer;
  set onValueContainer(int? onValueContainer) =>
      _$this._onValueContainer = onValueContainer;

  MaterialSelectionBuilder();

  MaterialSelectionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _value = $v.value;
      _onValue = $v.onValue;
      _valueContainer = $v.valueContainer;
      _onValueContainer = $v.onValueContainer;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MaterialSelection other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MaterialSelection;
  }

  @override
  void update(void Function(MaterialSelectionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$MaterialSelection build() {
    final _$result = _$v ??
        new _$MaterialSelection._(
            id: id,
            value: BuiltValueNullFieldError.checkNotNull(
                value, 'MaterialSelection', 'value'),
            onValue: BuiltValueNullFieldError.checkNotNull(
                onValue, 'MaterialSelection', 'onValue'),
            valueContainer: BuiltValueNullFieldError.checkNotNull(
                valueContainer, 'MaterialSelection', 'valueContainer'),
            onValueContainer: BuiltValueNullFieldError.checkNotNull(
                onValueContainer, 'MaterialSelection', 'onValueContainer'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
