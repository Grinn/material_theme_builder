import 'dart:async';

import 'package:color/color.dart';
import 'package:material_theme_builder/src/material_font.dart';
import 'package:material_theme_builder/src/material_theme.dart';
import 'package:source_gen/source_gen.dart';

class MaterialThemeGenerator extends Generator {
  FutureOr<String?> generateDartTheme(MaterialTheme _theme) async {
    final _class = '''
// ignore_for_file: always_use_package_imports
// GENERATED FILE, do not edit!
import 'package:color/color.dart';
import '../../template/template.dart';
import '../../template/template_palettes.dart';

class ${_theme.name}Theme {

  final Palettes _palettes;
  late MaterialThemeLight _light;
  late MaterialThemeDark _dark;
  final MaterialTextThemes _textThemes;
  ${_theme.name}Theme({Palettes? palettes})
      : _palettes = palettes ?? Palettes(),
        _textThemes = MaterialTextThemes() {
    _light = MaterialThemeLight(_palettes);
    _dark = MaterialThemeDark(_palettes);
  }

  factory ${_theme.name}Theme.fromTemplate(Template template) {
    MapEntry<int, int> _mapRgbColor(int key, RgbColor value) => MapEntry(
          key,
          int.parse('0xFF\${value.toHexColor()}'),
        );
    return ${_theme.name}Theme(
      palettes: TemplatePalettes(
        primary: template.primary.tones.map(_mapRgbColor),
        secondary: template.secondary.tones.map(_mapRgbColor),
        tertiary: template.tertiary.tones.map(_mapRgbColor),
        neutral: template.neutral.tones.map(_mapRgbColor),
        neturalVariant: template.neutralVariant.tones.map(_mapRgbColor),
      ),
    );
  }  


  Palettes get palettes => _palettes;
  MaterialThemeLight get light => _light;
  MaterialThemeDark get dark => _dark;
  MaterialTextThemes get textThemes => _textThemes;
}

class MaterialThemeLight {
  final Palettes palette;
  const MaterialThemeLight(this.palette);

  ${StringBuffer()..writeAll(
            _theme.palettes.keys
                .where((element) => !(element as String).startsWith('neutral'))
                .map((palette) {
              final _palette = palette as String;
              final _capitalPalette =
                  '${_palette[0].toUpperCase()}${_palette.substring(1)}';
              final _selection = _theme.colorPalettes
                  .firstWhere((element) => element.name == _palette);
              return '''\r
  // $_palette\r
  int get $_palette => palette.$_palette[${_selection.light.value}]!;
  int get on${_capitalPalette} => palette.$_palette[${_selection.light.onValue}]!;
  int get ${_palette}Container => palette.$_palette[${_selection.light.valueContainer}]!;
  int get on${_capitalPalette}Container => palette.$_palette[${_selection.light.onValueContainer}]!;\r
  ''';
            }).toList(),
          )}
  //background
  int get background => palette.neutral[${_theme.neutral.light.value}]!;
  int get onBackground => palette.neutral[${_theme.neutral.light.onValue}]!;

  //surface
  int get surface => palette.neutral[${_theme.neutral.light.valueContainer}]!;
  int get onSurface => palette.neutral[${_theme.neutral.light.onValueContainer}]!;

  //surfaceVariant
  int get surfaceVariant => palette.neutralVariant[${_theme.neutralVariant.light.value}]!;
  int get onSurfaceVariant => palette.neutralVariant[${_theme.neutralVariant.light.onValue}]!;
  int get outline => palette.neutralVariant[${_theme.neutralVariant.light.valueContainer}]!;
}

class MaterialThemeDark {
  final Palettes palette;
  const MaterialThemeDark(this.palette);

  ${StringBuffer()..writeAll(
            _theme.palettes.keys
                .where((element) => !(element as String).startsWith('neutral'))
                .map((palette) {
              final _palette = palette as String;
              final _capitalPalette =
                  '${_palette[0].toUpperCase()}${_palette.substring(1)}';
              final _selection = _theme.colorPalettes
                  .firstWhere((element) => element.name == _palette);
              return '''\r
  // $_palette\r
  int get $_palette => palette.$_palette[${_selection.dark.value}]!;
  int get on${_capitalPalette} => palette.$_palette[${_selection.dark.onValue}]!;
  int get ${_palette}Container => palette.$_palette[${_selection.dark.valueContainer}]!;
  int get on${_capitalPalette}Container => palette.$_palette[${_selection.dark.onValueContainer}]!;\r
  ''';
            }).toList(),
          )}
  //background
  int get background => palette.neutral[${_theme.neutral.dark.value}]!;
  int get onBackground => palette.neutral[${_theme.neutral.dark.onValue}]!;

  //surface
  int get surface => palette.neutral[${_theme.neutral.dark.valueContainer}]!;
  int get onSurface => palette.neutral[${_theme.neutral.dark.onValueContainer}]!;

  //surfaceVariant
  int get surfaceVariant => palette.neutralVariant[${_theme.neutralVariant.dark.value}]!;
  int get onSurfaceVariant => palette.neutralVariant[${_theme.neutralVariant.dark.onValue}]!;
  int get outline => palette.neutralVariant[${_theme.neutralVariant.dark.valueContainer}]!;
}

class Palettes {
    ${StringBuffer()..writeAll(_theme.palettes.keys.map((palette) {
            final _palette =
                palette == 'neutral-variant' ? 'neutralVariant' : palette;
            return '''
      Map<int, int> get $_palette => {
      ${StringBuffer()..writeAll(_theme.palettes[palette].keys.map((key) {
                    return '$key : ${_theme.palettes[palette][key]['int']},\r';
                  }).toList())}
    };
      ''';
          }).toList())}
} 

class MaterialTextThemes {
  ${_defaultFontStylesForDart(_theme)}

  ${StringBuffer()..writeAll(_theme.fonts.keys.map((font) {
            final _fontName = font as String;
            final _fontStyles = _theme.fonts[font];
            return '''MaterialTextTheme get ${_fontName[0].toLowerCase()}${_fontName.substring(1)} => MaterialTextTheme(
        ${StringBuffer()..writeAll(_fontStyles.keys.map((fontStyle) {
                    final _fontStyle = _fontStyles[fontStyle] as MaterialFont;
                    return '''\r
        $fontStyle : FontStyle(
        family: '${_fontStyle.family}',
        lineHeight: ${_fontStyle.lineHeight},
        weightName: '${_fontStyle.weight.toLowerCase()}',
        tracking: ${_fontStyle.tracking},
        size: ${_fontStyle.size},
        ),''';
                  }))}
          );'''
                '';
          }))}
}

class MaterialTextTheme {
  final FontStyle? bodyLarge;
  final FontStyle? bodyMedium;
  final FontStyle? bodySmall;
  final FontStyle? displayLarge;
  final FontStyle? displayMedium;
  final FontStyle? displaySmall;
  final FontStyle? headlineLarge;
  final FontStyle? headlineMedium;
  final FontStyle? headlineSmall;
  final FontStyle? labelLarge;
  final FontStyle? labelMedium;
  final FontStyle? labelSmall;
  final FontStyle? titleLarge;
  final FontStyle? titleMedium;
  final FontStyle? titleSmall;

  MaterialTextTheme({
    this.bodyLarge,
    this.bodyMedium,
    this.bodySmall,
    this.displayLarge,
    this.displayMedium,
    this.displaySmall,
    this.headlineLarge,
    this.headlineMedium,
    this.headlineSmall,
    this.labelLarge,
    this.labelMedium,
    this.labelSmall,
    this.titleLarge,
    this.titleMedium,
    this.titleSmall,
  });
}


class FontStyle {
  final String family;
  final double lineHeight;
  final String weightName;
  final double tracking;
  final double size;

  FontStyle({required this.family, required this.lineHeight, required this.weightName, required this.tracking, required this.size});
}
    ''';
    return _class;
  }

  String _defaultFontStylesForDart(MaterialTheme theme) {
    final _defaultFontStyles = _getDefaultFontStyles(theme);
    final _buffer = StringBuffer()
      ..write('''MaterialTextTheme get standard  => MaterialTextTheme(''')
      ..writeAll(_defaultFontStyles.map((fontStyle) {
        final _fontStyleName = fontStyle.keys.first;
        final _fontStyle = fontStyle.values.first as MaterialFont;
        return '''\r
        $_fontStyleName : FontStyle(
        family: '${_fontStyle.family}',
        lineHeight: ${_fontStyle.lineHeight},
        weightName: '${_fontStyle.weight.toString().toLowerCase()}',
        tracking: ${_fontStyle.tracking},
        size: ${_fontStyle.size},
        ),''';
      }).toList())
      ..write(');\r\n');
    return _buffer.toString();
  }

  FutureOr<String?> generateScssTheme(MaterialTheme _theme) async {
    return '''
// GENERATED FILE, do not edit!
${_importFonts(_theme)}

:root {
  //palettes
  ${StringBuffer()..writeAll(_theme.palettes.keys.map((palette) {
            return StringBuffer()
              ..writeAll(_theme.palettes[palette].keys.map((key) {
                final _color = _theme.palettes[palette][key]['hex'] as String;
                final _hex = HexColor(_color.replaceAll('#', ''));
                final _rgb = RgbColor(_hex.r, _hex.g, _hex.b);
                return '--mdc-theme-$palette-$key : ${_color};\r--mdc-theme-$palette-$key-rgb : ${_rgb.toCssString()};\r';
              }).toList())
              ..writeln('\n');
          }).toList())}
    
  //light
  ${StringBuffer()..writeAll(_theme.palettes.keys.map((palette) {
            return '''
  // $palette\r
  --mdc-theme-$palette: ${_theme.palettes[palette][_theme.palette(palette).light.value]['hex']};\r
  --mdc-theme-on-$palette: ${_theme.palettes[palette][_theme.palette(palette).light.onValue]['hex']};\r
  --mdc-theme-${palette}-container: ${_theme.palettes[palette][_theme.palette(palette).light.valueContainer]['hex']};\r
  --mdc-theme-on-${palette}-container: ${_theme.palettes[palette][_theme.palette(palette).light.onValueContainer]['hex']};\r
  ''';
          }).toList())}          
  //background
  --mdc-theme-background: ${_theme.palettes['neutral'][_theme.neutral.light.value]['hex']};\r
  --mdc-theme-on-background: ${_theme.palettes['neutral'][_theme.neutral.light.onValue]['hex']};\r
  //surface
  --mdc-theme-surface: ${_theme.palettes['neutral'][_theme.neutral.light.valueContainer]['hex']};\r
  --mdc-theme-on-surface: ${_theme.palettes['neutral'][_theme.neutral.light.onValueContainer]['hex']};\r
  //surfaceVariant
  --mdc-theme-surface-variant: ${_theme.palettes['neutralVariant'][_theme.neutralVariant.light.value]['hex']};\r
  --mdc-theme-on-surface-variant: ${_theme.palettes['neutralVariant'][_theme.neutralVariant.light.onValue]['hex']};\r
  --mdc-theme-outline: ${_theme.palettes['neutralVariant'][_theme.neutralVariant.light.valueContainer]['hex']};\r
  
  //dark
    ${StringBuffer()..writeAll(_theme.palettes.keys.map((palette) {
            return '''
  // $palette\r
  --mdc-theme-dark-$palette: ${_theme.palettes[palette][_theme.palette(palette).dark.value]['hex']};\r
  --mdc-theme-dark-on-$palette: ${_theme.palettes[palette][_theme.palette(palette).dark.onValue]['hex']};\r
  --mdc-theme-dark-${palette}-container: ${_theme.palettes[palette][_theme.palette(palette).dark.valueContainer]['hex']};\r
  --mdc-theme-dark-on-${palette}-container: ${_theme.palettes[palette][_theme.palette(palette).dark.onValueContainer]['hex']};\r
  ''';
          }).toList())}
  //background
  --mdc-theme-dark-background: ${_theme.palettes['neutral'][_theme.neutral.dark.value]['hex']};\r
  --mdc-theme-dark-on-background: ${_theme.palettes['neutral'][_theme.neutral.dark.onValue]['hex']};\r
  //surface
  --mdc-theme-dark-surface: ${_theme.palettes['neutral'][_theme.neutral.dark.valueContainer]['hex']};\r
  --mdc-theme-dark-on-surface: ${_theme.palettes['neutral'][_theme.neutral.light.onValueContainer]['hex']};\r
  //surfaceVariant
  --mdc-theme-dark-surface-variant: ${_theme.palettes['neutralVariant'][_theme.neutralVariant.dark.value]['hex']};\r
  --mdc-theme-dark-on-surface-variant: ${_theme.palettes['neutralVariant'][_theme.neutralVariant.dark.onValue]['hex']};\r
  --mdc-theme-dark-outline: ${_theme.palettes['neutralVariant'][_theme.neutralVariant.dark.valueContainer]['hex']};\r
}
    ${_defaultFontStylesForCss(_theme)} 
   
    ${StringBuffer()..writeAll(_theme.fonts.keys.map((fontFamilyName) {
            final _fontStyles = _theme.fonts[fontFamilyName].keys;

            return _fontStyles.map((fontStyle) {
              final _fontStyle =
                  _theme.fonts[fontFamilyName][fontStyle] as MaterialFont;
              return '''\r
  @mixin $fontStyle$fontFamilyName {
  font-family: '${_fontStyle.family}', sans-serif;\r
  line-height: ${_fontStyle.lineHeight}px;\r
  font-weight: ${_fontWeightByName(_fontStyle.weight.toString().toLowerCase())};\r
  font-size: ${_fontStyle.size}px;\r
  }''';
            }).toList();
          }).expand((element) => element))}
''';
  }

  String _importFonts(MaterialTheme theme) {
    final _fontFamilyNames = <String>{};
    final _fontWeights = <String, Set<int>>{};
    _getDefaultFontStyles(theme).forEach((fontStyle) {
      for (final _fontStyle in fontStyle.values) {
        _fontFamilyNames.add(_fontStyle.family);
        if (_fontWeights[_fontStyle.family] == null) {
          _fontWeights[_fontStyle.family] = <int>{};
        }
        _fontWeights[_fontStyle.family]
            ?.add(_fontWeightByName(_fontStyle.weight));
      }
    });

    _fontFamilyNames.toSet().toList();
    final _importUrls = _fontFamilyNames.map((fontFamilyName) {
      String _weights = '';
      if (_fontWeights[fontFamilyName] != null) {
        final _sortedWeights = _fontWeights[fontFamilyName]!.toList();
        _sortedWeights.sort((lhs, rhs) => lhs.compareTo(rhs));
        _weights = ':wght@${_sortedWeights.join(';')}';
      }
      return "@import url('https://fonts.googleapis.com/css2?family=${fontFamilyName.replaceAll(' ', '+')}$_weights&display=swap');";
    }).join('\r');
    return _importUrls;
  }

  String _defaultFontStylesForCss(MaterialTheme theme) {
    final _defaultFontStyles = _getDefaultFontStyles(theme);
    final _buffer = StringBuffer()
      ..writeAll(_defaultFontStyles.map((fontStyle) {
        final _fontStyleName = fontStyle.keys.first;
        final _fontStyle = fontStyle.values.first as MaterialFont;
        return '''\r
    @mixin $_fontStyleName {
    font-family: '${_fontStyle.family}', sans-serif;\r
    line-height: ${_fontStyle.lineHeight}px;\r
    font-weight: ${_fontWeightByName(_fontStyle.weight.toString().toLowerCase())};\r
    font-size: ${_fontStyle.size}px;\r
    }''';
      }).toList())
      ..write('\r\n');
    return _buffer.toString();
  }

  List<Map<dynamic, dynamic>> _getDefaultFontStyles(MaterialTheme theme) {
    late List<Map<dynamic, dynamic>> _defaultFontStyles;
    if (theme.fonts.length > 1) {
      _defaultFontStyles = _findDefaultFontStyleByToken(theme);
    } else {
      _defaultFontStyles = _mapDefaultStyles(theme);
    }
    return _defaultFontStyles;
  }

  List<Map<dynamic, dynamic>> _mapDefaultStyles(MaterialTheme theme) {
    final _fontFamilyName = theme.fonts.keys.first;
    final _fontStyles = theme.fonts[_fontFamilyName] as Map;
    List<Map<dynamic, dynamic>> _defaultFontStyles = [];
    _fontStyles.forEach((name, fontStyle) {
      _defaultFontStyles.add({name: fontStyle});
    });
    return _defaultFontStyles;
  }

  List<Map<dynamic, dynamic>> _findDefaultFontStyleByToken(
      MaterialTheme theme) {
    final _requiredTypes = ['body', 'display', 'headline', 'label', 'title'];
    final _requiredVariants = ['Large', 'Medium', 'Small'];
    final _defaultFontStyles = theme.fonts.keys
        .map((fontFamilyName) {
          final fontStyleNames =
              theme.fonts[fontFamilyName].keys as Iterable<dynamic>;
          return fontStyleNames.where((fontStyle) {
            final font = theme.fonts[fontFamilyName][fontStyle] as MaterialFont;
            return font.isDefault == 'true';
          }).map((fontStyleName) =>
              {fontStyleName: theme.fonts[fontFamilyName][fontStyleName]});
        })
        .where((element) => element.isNotEmpty)
        .expand((element) => element);

    final _missingFontStyles = _requiredTypes.map((requiredType) {
      return _requiredVariants.map((requiredVariant) {
        final _requiredFontStyle = '$requiredType$requiredVariant';
        final _fontStyleIsNotPresent = _defaultFontStyles.every((fontStyle) =>
            (fontStyle[_requiredFontStyle] as MaterialFont?)?.isDefault ==
            null);
        return _fontStyleIsNotPresent ? _requiredFontStyle : null;
      }).where((element) => element != null);
    }).expand((element) => element);
    if (_missingFontStyles.isNotEmpty) {
      print(
          'missing default font styles (${_missingFontStyles.length}): $_missingFontStyles');
    }
    return _defaultFontStyles.toList();
  }

  int _fontWeightByName(String name) {
    switch (name.toLowerCase()) {
      case 'thin':
        return 100;
      case 'extralight':
        return 200;
      case 'light':
        return 300;
      case 'regular':
        return 400;
      case 'medium':
        return 500;
      case 'semibold':
        return 600;
      case 'bold':
        return 700;
      case 'extrabold':
        return 800;
      case 'black':
        return 900;
      default:
        return 400;
    }
  }

  FutureOr<String?> generateColorsMixin(MaterialTheme _theme) async {
    return '''
  // GENERATED FILE, do not edit!

  //palettes
  ${StringBuffer()..writeAll(_theme.palettes.keys.map((palette) {
            return StringBuffer()
              ..writeAll(_theme.palettes[palette].keys.map((key) {
                final _color = _theme.palettes[palette][key]['hex'] as String;
                final _hex = HexColor(_color.replaceAll('#', ''));
                final _rgb = RgbColor(_hex.r, _hex.g, _hex.b);
                return '\$$palette-$key : ${_color};\r\$$palette-$key-rgb : ${_rgb.toCssString()};\r';
              }).toList())
              ..writeln('\n');
          }).toList())}
    
  //light
  ${StringBuffer()..writeAll(_theme.palettes.keys.map((palette) {
            return '''
  // $palette\r
  \$$palette: ${_theme.palettes[palette][_theme.palette(palette).light.value]['hex']};\r
  \$on-$palette: ${_theme.palettes[palette][_theme.palette(palette).light.onValue]['hex']};\r
  \$${palette}-container: ${_theme.palettes[palette][_theme.palette(palette).light.valueContainer]['hex']};\r
  \$on-${palette}-container: ${_theme.palettes[palette][_theme.palette(palette).light.onValueContainer]['hex']};\r
  ''';
          }).toList())}          
  //background
  \$background: ${_theme.palettes['neutral'][_theme.neutral.light.value]['hex']};\r
  \$on-background: ${_theme.palettes['neutral'][_theme.neutral.light.onValue]['hex']};\r
  //surface
  \$surface: ${_theme.palettes['neutral'][_theme.neutral.light.valueContainer]['hex']};\r
  \$on-surface: ${_theme.palettes['neutral'][_theme.neutral.light.onValueContainer]['hex']};\r
  //surfaceVariant
  \$surface-variant: ${_theme.palettes['neutralVariant'][_theme.neutralVariant.light.value]['hex']};\r
  \$on-surface-variant: ${_theme.palettes['neutralVariant'][_theme.neutralVariant.light.onValue]['hex']};\r
  \$outline: ${_theme.palettes['neutralVariant'][_theme.neutralVariant.light.valueContainer]['hex']};\r
  
  //dark
    ${StringBuffer()..writeAll(_theme.palettes.keys.map((palette) {
            return '''
  // $palette\r
  \$dark-$palette: ${_theme.palettes[palette][_theme.palette(palette).dark.value]['hex']};\r
  \$dark-on-$palette: ${_theme.palettes[palette][_theme.palette(palette).dark.onValue]['hex']};\r
  \$dark-${palette}-container: ${_theme.palettes[palette][_theme.palette(palette).dark.valueContainer]['hex']};\r
  \$dark-on-${palette}-container: ${_theme.palettes[palette][_theme.palette(palette).dark.onValueContainer]['hex']};\r
  ''';
          }).toList())}
  //background
  \$dark-background: ${_theme.palettes['neutral'][_theme.neutral.dark.value]['hex']};\r
  \$dark-on-background: ${_theme.palettes['neutral'][_theme.neutral.dark.onValue]['hex']};\r
  //surface
  \$dark-surface: ${_theme.palettes['neutral'][_theme.neutral.dark.valueContainer]['hex']};\r
  \$dark-on-surface: ${_theme.palettes['neutral'][_theme.neutral.light.onValueContainer]['hex']};\r
  //surfaceVariant
  \$dark-surface-variant: ${_theme.palettes['neutralVariant'][_theme.neutralVariant.dark.value]['hex']};\r
  \$dark-on-surface-variant: ${_theme.palettes['neutralVariant'][_theme.neutralVariant.dark.onValue]['hex']};\r
  \$dark-outline: ${_theme.palettes['neutralVariant'][_theme.neutralVariant.dark.valueContainer]['hex']};\r
''';
  }

  FutureOr<String?> generateTextMixin(MaterialTheme _theme) async {
    return '''
// GENERATED FILE, do not edit!
${_importFonts(_theme)}

${_defaultFontStylesForCss(_theme)} 

${StringBuffer()..writeAll(_theme.fonts.keys.map((fontFamilyName) {
            final _fontStyles = _theme.fonts[fontFamilyName].keys;

            return _fontStyles.map((fontStyle) {
              final _fontStyle =
                  _theme.fonts[fontFamilyName][fontStyle] as MaterialFont;
              return '''\r
@mixin $fontStyle$fontFamilyName {
font-family: '${_fontStyle.family}', sans-serif;\r
line-height: ${_fontStyle.lineHeight}px;\r
font-weight: ${_fontWeightByName(_fontStyle.weight.toString().toLowerCase())};\r
font-size: ${_fontStyle.size}px;\r
}''';
            }).toList();
          }).expand((element) => element))}
''';
  }
}
