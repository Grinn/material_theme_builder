// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'material_font.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<MaterialFont> _$materialFontSerializer =
    new _$MaterialFontSerializer();

class _$MaterialFontSerializer implements StructuredSerializer<MaterialFont> {
  @override
  final Iterable<Type> types = const [MaterialFont, _$MaterialFont];
  @override
  final String wireName = 'MaterialFont';

  @override
  Iterable<Object?> serialize(Serializers serializers, MaterialFont object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'family',
      serializers.serialize(object.family,
          specifiedType: const FullType(String)),
      'size',
      serializers.serialize(object.size, specifiedType: const FullType(String)),
      'weight',
      serializers.serialize(object.weight,
          specifiedType: const FullType(String)),
      'lineHeight',
      serializers.serialize(object.lineHeight,
          specifiedType: const FullType(String)),
      'tracking',
      serializers.serialize(object.tracking,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.isDefault;
    if (value != null) {
      result
        ..add('isDefault')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  MaterialFont deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MaterialFontBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'family':
          result.family = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'size':
          result.size = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'weight':
          result.weight = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lineHeight':
          result.lineHeight = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'tracking':
          result.tracking = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'isDefault':
          result.isDefault = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$MaterialFont extends MaterialFont {
  @override
  final String family;
  @override
  final String size;
  @override
  final String weight;
  @override
  final String lineHeight;
  @override
  final String tracking;
  @override
  final String? isDefault;

  factory _$MaterialFont([void Function(MaterialFontBuilder)? updates]) =>
      (new MaterialFontBuilder()..update(updates)).build();

  _$MaterialFont._(
      {required this.family,
      required this.size,
      required this.weight,
      required this.lineHeight,
      required this.tracking,
      this.isDefault})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(family, 'MaterialFont', 'family');
    BuiltValueNullFieldError.checkNotNull(size, 'MaterialFont', 'size');
    BuiltValueNullFieldError.checkNotNull(weight, 'MaterialFont', 'weight');
    BuiltValueNullFieldError.checkNotNull(
        lineHeight, 'MaterialFont', 'lineHeight');
    BuiltValueNullFieldError.checkNotNull(tracking, 'MaterialFont', 'tracking');
  }

  @override
  MaterialFont rebuild(void Function(MaterialFontBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MaterialFontBuilder toBuilder() => new MaterialFontBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MaterialFont &&
        family == other.family &&
        size == other.size &&
        weight == other.weight &&
        lineHeight == other.lineHeight &&
        tracking == other.tracking &&
        isDefault == other.isDefault;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, family.hashCode), size.hashCode),
                    weight.hashCode),
                lineHeight.hashCode),
            tracking.hashCode),
        isDefault.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MaterialFont')
          ..add('family', family)
          ..add('size', size)
          ..add('weight', weight)
          ..add('lineHeight', lineHeight)
          ..add('tracking', tracking)
          ..add('isDefault', isDefault))
        .toString();
  }
}

class MaterialFontBuilder
    implements Builder<MaterialFont, MaterialFontBuilder> {
  _$MaterialFont? _$v;

  String? _family;
  String? get family => _$this._family;
  set family(String? family) => _$this._family = family;

  String? _size;
  String? get size => _$this._size;
  set size(String? size) => _$this._size = size;

  String? _weight;
  String? get weight => _$this._weight;
  set weight(String? weight) => _$this._weight = weight;

  String? _lineHeight;
  String? get lineHeight => _$this._lineHeight;
  set lineHeight(String? lineHeight) => _$this._lineHeight = lineHeight;

  String? _tracking;
  String? get tracking => _$this._tracking;
  set tracking(String? tracking) => _$this._tracking = tracking;

  String? _isDefault;
  String? get isDefault => _$this._isDefault;
  set isDefault(String? isDefault) => _$this._isDefault = isDefault;

  MaterialFontBuilder();

  MaterialFontBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _family = $v.family;
      _size = $v.size;
      _weight = $v.weight;
      _lineHeight = $v.lineHeight;
      _tracking = $v.tracking;
      _isDefault = $v.isDefault;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MaterialFont other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MaterialFont;
  }

  @override
  void update(void Function(MaterialFontBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$MaterialFont build() {
    final _$result = _$v ??
        new _$MaterialFont._(
            family: BuiltValueNullFieldError.checkNotNull(
                family, 'MaterialFont', 'family'),
            size: BuiltValueNullFieldError.checkNotNull(
                size, 'MaterialFont', 'size'),
            weight: BuiltValueNullFieldError.checkNotNull(
                weight, 'MaterialFont', 'weight'),
            lineHeight: BuiltValueNullFieldError.checkNotNull(
                lineHeight, 'MaterialFont', 'lineHeight'),
            tracking: BuiltValueNullFieldError.checkNotNull(
                tracking, 'MaterialFont', 'tracking'),
            isDefault: isDefault);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
