import 'package:material_theme_builder/src/material_font_style.dart';
import 'package:material_theme_builder/src/material_palette.dart';

class MaterialTheme {
  MaterialTheme({
    required this.palettes,
    required this.fonts,
    required this.colorPalettes,
    required this.name,
  });

  final String name;
  final Map palettes;
  final List<MaterialPalette> colorPalettes;
  final Map fonts;

  MaterialPalette get neutral => palette('neutral');

  MaterialPalette get neutralVariant => palette('neutralVariant');

  MaterialPalette palette(String name) {
    late String _name;
    if (name.contains('-')) {
      final _nameParts = name.split('-');
      _name =
          '${_nameParts.first}${_nameParts.last[0].toUpperCase()}${_nameParts.last.substring(1)}';
    } else {
      _name = name;
    }
    return colorPalettes.firstWhere((element) => element.name == _name);
  }

  factory MaterialTheme.fromJson(Map json) {
    final _colors = (json['colors'] as List<dynamic>)
        .map((color) => MaterialPalette.fromJson(color))
        .toList();
    _colors.sort((lhs, rhs) => lhs.name.compareTo(rhs.name));
    final _palettes = Map.fromEntries(
        _colors.map((color) => MapEntry(color.name, color.tones)));

    final _fonts = (json['fonts'] as List<dynamic>)
        .map((font) => MaterialFontStyle.fromJson(font))
        .toList();
    _fonts.sort((lhs, rhs) {
      return lhs.name.compareTo(rhs.name);
    });

    final _textThemes = {};
    for (final _style in _fonts) {
      for (final _font in _style.fonts) {
        final _familyName = _font.family.replaceAll(RegExp(r'[^a-zA-Z]'), '');
        if (_textThemes[_familyName] == null) {
          _textThemes[_familyName] = {};
        }
        final _fontStyle = _style.fontStyle(_font);
        _textThemes[_familyName][_fontStyle] = _font;
      }
    }
    return MaterialTheme(
      palettes: _palettes,
      fonts: _textThemes,
      colorPalettes: _colors,
      name: json['name'],
    );
  }
}
