/// Support for doing something awesome.
///
/// More dartdocs go here.
library material_theme_builder;

import 'package:build/build.dart';
import 'package:material_theme_builder/src/material_theme_builder.dart';
import 'package:material_theme_builder/src/material_theme_generator.dart';

Builder materialThemeBuilder(BuilderOptions options) =>
    MaterialThemeBuilder(MaterialThemeGenerator());
