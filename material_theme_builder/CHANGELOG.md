## 1.0.6
- Simplified input json for fonts

## 1.0.5
- Corrected palette reference

## 1.0.4
- Simplify input json by generating material palette colors

## 1.0.3
- Generate default text theme

## 1.0.2
- Allow multiple fonts to be used

## 1.0.1
- Parse which tones should be used 

## 1.0.0
- Initial version.
